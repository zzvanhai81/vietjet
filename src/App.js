import { Table } from "antd";
import { sortBy } from "lodash";
import { listFlight } from "./data";
const Column = Table.Column;

function App() {
  return (
    <div className="App">
      <Table
        style={{
          minWidth: "303px",
          cursor: "pointer",
        }}
        size="small"
        bordered
        dataSource={sortBy(listFlight, [
          "departureDate",
          "departureTime",
        ]).filter((item) => {
          let getDay = item.departureDate.split("-");
          let getHour = item.departureTime.split(":");
          let temp = new Date(
            parseInt(getDay[2]),
            parseInt(getDay[1]) - 1,
            parseInt(getDay[0]),
            parseInt(getHour[0]),
            parseInt(getHour[1]),
            parseInt(getHour[2])
          );
          let dayMoment = new Date().toUTCString();
          let dayData = new Date(temp).toUTCString();
          console.log("/ChangeFlightAPI/", dayData);
          console.log("/ChangeFlightMoment/", dayMoment);
          if (
            new Date(dayData).getTime() > new Date(dayMoment).getTime() &&
            item.informationSeat.available > 0
          ) {
            return item;
          }
        })}
        pagination={true}
        rowKey="travelOptionKey"
      >
        <Column
          title="Flight No."
          dataIndex="flightNumber"
          key="flightNo"
          align="center"
        />
        <Column
          title="Departure Date"
          dataIndex="departureDate"
          key="DepartureDate"
          align="center"
        />
        <Column
          title="Departure Time"
          dataIndex="departureTime"
          key="DepartureTime"
          align="center"
        />
      </Table>
    </div>
  );
}

export default App;
